import './App.css';
import Feed from './Feed';
import Header from './Header';
import Sidebar from './Sidebar';
import Widgets from './Widgets';

function App() {
  const user = null;

  return (
    <div className="app">
      {!user ? (
      <h1>Login</h1>
      ) : (
      <>
        {/* Header */}
        {/* Ctrl+K+C/Ctrl+K+U */}
        <Header />
        {/* App body */}
        <div className="app__body">
          {/* Sidebar */}
          <Sidebar />
          {/* Feed */}
          <Feed />
          {/* Widgets */}
          <Widgets />
        </div>
      </>
      )}
    </div>
  );
}

export default App;
