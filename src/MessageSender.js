import { Avatar } from '@material-ui/core';
import { InsertEmoticon, PhotoLibrary, Videocam } from '@material-ui/icons';
import React, { useState } from 'react';
import "./MessageSender.css"

function MessageSender() {

    const [input, setInput] = useState("");
    const [imageUrl, setImageUrl] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();

        setInput("");
        setImageUrl("");
    }

    return (
        <div className="messageSender">
            <div className='messageSender__top'>
                <Avatar src='https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/54520500_2303411149872236_91824888959991808_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=YjfdlFu4CqgAX_GsrOf&_nc_ht=scontent.fhan2-4.fna&oh=f19a52b8fdd6e861efb6967dffcfc57d&oe=6045CB58' />
                <form>
                    <input
                     value={input}
                     onChange={(e) => setInput(e.target.value)}
                     className='messageSender__input' 
                     placeholder={`What's on your mind?`} />
                    <input
                     value={imageUrl}
                     onChange={(e) => setImageUrl(e.target.value)}
                     type='text'
                     placeholder="image URL (Optional)" />
                    <button onClick={handleSubmit} type="submit">
                        Hidden submit
                    </button>
                </form>
            </div>
            <div className='messageSender__bottom'>
                <div className='messageSender__option'>
                    <Videocam style={{ color: "red" }} />
                    <h3>Live Video</h3>
                </div>

                <div className='messageSender__option'>
                    <PhotoLibrary style={{ color: "green" }} />
                    <h3>Live Video</h3>
                </div>

                <div className='messageSender__option'>
                    <InsertEmoticon style={{ color: "orange" }} />
                    <h3>Feeling/Activity</h3>
                </div>
            </div>
        </div>
    )
}

export default MessageSender
