import { Chat, EmojiFlags, ExpandMoreOutlined, LocalHospital, People, Storefront, VideoLibrary } from '@material-ui/icons';
import React from 'react';
import "./Sidebar.css";

import SidebarRow from "./SidebarRow";

const user = {
    photoURL: "https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/54520500_2303411149872236_91824888959991808_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=YjfdlFu4CqgAX_GsrOf&_nc_ht=scontent.fhan2-4.fna&oh=f19a52b8fdd6e861efb6967dffcfc57d&oe=6045CB58",
    displayName: "Hoa Pikey"
}

function Sidebar() {
    return (
        <div className="sidebar">
            <SidebarRow src={user.photoURL} title={user.displayName} />
            <SidebarRow Icon={LocalHospital} title="COVID-19 Information Center" />
            <SidebarRow Icon={EmojiFlags} title='Pages'/>
            <SidebarRow Icon={People} title='Friends'/>
            <SidebarRow Icon={Chat} title='Messenger'/>
            <SidebarRow Icon={Storefront} title='Marketplace'/>
            <SidebarRow Icon={VideoLibrary} title='Videos'/>
            <SidebarRow Icon={ExpandMoreOutlined} title='Mraketplace'/>
        </div>
    )
}

export default Sidebar
