import React from 'react';
import "./Widgets.css"

function Widgets() {
    return (
        <div className='widgets'>
            <iframe
             src="https://www.sololearn.com/Discuss/1966718/iframe-refused-to-connect"
             width="340"
             height="100%"
             style={{ border: "none", overflow: "hidden" }}
             scrolling="no"
             frameBorder="0"
             allowTransparency="true"
             allow="encrypted-media"
             title="unique"
            ></iframe>
        </div>
    )
}

export default Widgets
