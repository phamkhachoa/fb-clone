import React from 'react';
import "./Feed.css";
import MessageSender from './MessageSender';
import Post from './Post';
import StoryReel from './StoryReel';

export default function Feed() {
    return (
        <div className='feed'>
            {/* StoryReel */}
            <StoryReel />
            {/* MessageSender */}
            <MessageSender />
            <Post
              profilePic="https://scontent.fhan2-5.fna.fbcdn.net/v/t1.0-9/143195943_1140575276400552_6955432923483936908_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_ohc=utdK7Au72goAX_vW4Zw&_nc_ht=scontent.fhan2-5.fna&oh=c19ea98bc7de281d8e19f091b634441a&oe=6046DF16"
              message="WOW this work!"
              timestamp="This is a timestamp"
              username='Linh Linh'
              image="https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/145801711_1147689495689130_2365742449086087624_o.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_ohc=FDX1bLxyU-oAX-2Uqri&_nc_ht=scontent.fhan2-6.fna&oh=6192af89284babb50b5a83afa437af91&oe=6046591F"
            />
            <Post
              profilePic="https://scontent.fhan2-5.fna.fbcdn.net/v/t1.0-9/143195943_1140575276400552_6955432923483936908_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_ohc=utdK7Au72goAX_vW4Zw&_nc_ht=scontent.fhan2-5.fna&oh=c19ea98bc7de281d8e19f091b634441a&oe=6046DF16"
              message="WOW this work!"
              timestamp="This is a timestamp"
              username='Linh Linh'
              image="https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/145801711_1147689495689130_2365742449086087624_o.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_ohc=FDX1bLxyU-oAX-2Uqri&_nc_ht=scontent.fhan2-6.fna&oh=6192af89284babb50b5a83afa437af91&oe=6046591F"
            />
            <Post
              profilePic="https://scontent.fhan2-5.fna.fbcdn.net/v/t1.0-9/143195943_1140575276400552_6955432923483936908_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_ohc=utdK7Au72goAX_vW4Zw&_nc_ht=scontent.fhan2-5.fna&oh=c19ea98bc7de281d8e19f091b634441a&oe=6046DF16"
              message="WOW this work!"
              timestamp="This is a timestamp"
              username='Linh Linh'
              image="https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/145801711_1147689495689130_2365742449086087624_o.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_ohc=FDX1bLxyU-oAX-2Uqri&_nc_ht=scontent.fhan2-6.fna&oh=6192af89284babb50b5a83afa437af91&oe=6046591F"
            />
        </div>
    )
}
