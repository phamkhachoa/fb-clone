import React from 'react';
import "./Header.css";
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import FlagIcon from '@material-ui/icons/Flag';
import AddIcon from '@material-ui/icons/Add';
import ForumIcon from '@material-ui/icons/Forum';
import NotificationsActive from '@material-ui/icons/NotificationsActive';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { StorefrontOutlined, SubscriptionsOutlined, SupervisedUserCircle } from '@material-ui/icons';
import { Avatar, IconButton } from '@material-ui/core';

function Header() {
    return (
        <div className='header'>
            <div className='header_left'>
                <img src={`${process.env.PUBLIC_URL}/facebook-social-media-fb-logo-square-44659.png`} alt="" />
                <div className="header_input">
                    <SearchIcon />
                    <input type="text" placeholder="Search Something"/>
                </div>
            </div>

            <div className='header_center'>
                <div className="header_option header_option--active">
                    <HomeIcon fontSize="large" />
                </div>

                <div className="header_option">
                    <FlagIcon fontSize="large" />
                </div>

                <div className="header_option">
                    <SubscriptionsOutlined fontSize="large" />
                </div>

                <div className="header_option">
                    <StorefrontOutlined fontSize="large" />
                </div>

                <div className="header_option">
                    <SupervisedUserCircle fontSize="large" />
                </div>
            </div>

            <div className='header_right'>
                <div className="header_info">
                    <Avatar src='https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/54520500_2303411149872236_91824888959991808_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=YjfdlFu4CqgAX_GsrOf&_nc_ht=scontent.fhan2-4.fna&oh=f19a52b8fdd6e861efb6967dffcfc57d&oe=6045CB58'/>
                    <h4>Hoa Pikey</h4>
                </div>

                <IconButton>
                    <AddIcon />
                </IconButton>

                <IconButton>
                    <ForumIcon />
                </IconButton>

                <IconButton>
                    <NotificationsActive />
                </IconButton>

                <IconButton>
                    <ExpandMore />
                </IconButton>
            </div>
        </div>
    )
}

export default Header
