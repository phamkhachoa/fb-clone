import React from 'react';
import Story from './Story';
import "./StoryReel.css"

function StoryReel() {
    return (
        <div className='storyReel'>
            {/* Story */}
            <Story profileSrc='https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/54520500_2303411149872236_91824888959991808_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=YjfdlFu4CqgAX_GsrOf&_nc_ht=scontent.fhan2-4.fna&oh=f19a52b8fdd6e861efb6967dffcfc57d&oe=6045CB58'
                image='https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/s1080x2048/146093805_1147611589030254_6752739228290548698_o.jpg?_nc_cat=108&ccb=2&_nc_sid=5b7eaf&_nc_ohc=dIquQXpoKMEAX8Ry27P&_nc_ht=scontent.fhan2-3.fna&tp=7&oh=ee496c0bd47edcf9428e1a440270bd2b&oe=604485B2'
                title='Linh Linh'
            />
            <Story profileSrc='https://scontent.fhan2-5.fna.fbcdn.net/v/t1.0-9/143195943_1140575276400552_6955432923483936908_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_ohc=utdK7Au72goAX_vW4Zw&_nc_ht=scontent.fhan2-5.fna&oh=c19ea98bc7de281d8e19f091b634441a&oe=6046DF16'
                image='https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/108851409_989363791521702_6872025313810876492_o.jpg?_nc_cat=100&ccb=2&_nc_sid=8bfeb9&_nc_ohc=6OIfBroJdYgAX-M4O9C&_nc_ht=scontent.fhan2-6.fna&oh=416f096168a02978a38be9a20a02aa05&oe=60473681'
                title='Linh Linh'
            />
            <Story profileSrc='https://scontent.fhan2-5.fna.fbcdn.net/v/t1.0-9/143195943_1140575276400552_6955432923483936908_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_ohc=utdK7Au72goAX_vW4Zw&_nc_ht=scontent.fhan2-5.fna&oh=c19ea98bc7de281d8e19f091b634441a&oe=6046DF16'
                image='https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/134850294_1123245534800193_3360266891212907287_o.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_ohc=bJLaPrfV4PsAX-7alP7&_nc_ht=scontent.fhan2-6.fna&oh=5d3cc08ed2087c9345f7e0b9bc0f88ad&oe=6045C8CD'
                title='Linh Linh'
            />
            <Story profileSrc='https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/125835102_1094820944309319_7481309049010727126_o.jpg?_nc_cat=100&ccb=2&_nc_sid=8bfeb9&_nc_ohc=inl-YYiAYHQAX-ICeat&_nc_ht=scontent.fhan2-6.fna&oh=258f8cd293b7f6d1c46e5812bbd98f9f&oe=604423CB'
                image='https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/57154220_658989221225829_3812535794556469248_o.jpg?_nc_cat=105&ccb=2&_nc_sid=8bfeb9&_nc_ohc=1lmGr-WGGRwAX9u07or&_nc_ht=scontent.fhan2-4.fna&oh=cb8a367c600746923b8a4a951ec0ad22&oe=60452DD5'
                title='Linh Linh'
            />
            <Story profileSrc='https://scontent.fhan2-5.fna.fbcdn.net/v/t1.0-9/143195943_1140575276400552_6955432923483936908_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_ohc=utdK7Au72goAX_vW4Zw&_nc_ht=scontent.fhan2-5.fna&oh=c19ea98bc7de281d8e19f091b634441a&oe=6046DF16'
                image='https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/121734904_2884900151743157_4331302933642036764_o.jpg?_nc_cat=110&ccb=2&_nc_sid=174925&_nc_ohc=h9fHLqTOHrwAX9qVs4l&_nc_ht=scontent.fhan2-4.fna&oh=5ce9a8823f2cf15184e32c347e1c42d9&oe=6045D3DA'
                title='Linh Ngọc'
            />
        </div>
    )
}

export default StoryReel
